Ntpd
#####################
:date: 2015-08-06
:category: RedHat, Centos, Ntpd

Overview
--------
  Ansible-роль для установки и настройки ntpd-сервера 
  Настройка определяется переменными ``ntpd_serverpool`` и ``ntpd_restrict``.
  ``serverpool`` - предназначена для задания пула-серверов, с которых брать время
  ``restrict``   - определяет для каких подсетей "отдавать" текущее время (в случаях работы ntpd, как проксирующего сервера времени)

How to use
----------
ntpd.yml::
  - name: ntpd configure
    hosts: all
    sudo: yes
    gather_facts: no
    roles: 
    - { role: Ntpd,
      }
Вызов::
  ansible-playbook -v -i inventory/hosts.ini ntpd.yml --limit ntp-local-servers -K
  or
  ansible-playbook -v -i inventory/hosts.ini ntpd.yml --limit ntp-local-clients -K

group_vars
------------
Для группы ntp-local-servers, определенной в inventory-файле, заданы следующие значения в файле ``group_vars/ntp-local-servers.yml``::
  ntpd_restrict: [ 
    { net: "10.77.171.0", mask: "255.255.255.0" }, 
    { net: "10.77.172.0", mask: "255.255.255.0" }
  ]

Для группы ntp-local-clients, определенной в inventory-файле, заданы следующие значения в файле ``group_vars/ntp-local-clients.yml``::
  ntpd_serverpool: [ 
    "10.77.171.21", 
    "10.77.171.22",
    "10.77.171.23",
    "10.77.171.24",
    "10.77.171.25",
    "10.77.171.26",
    "10.77.171.27" ]
